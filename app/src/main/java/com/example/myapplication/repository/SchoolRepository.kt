package com.example.myapplication.repository

import com.example.myapplication.model.School
import com.example.myapplication.retrofit.NycSchoolData

class SchoolRepository (private val schoolDataService: NycSchoolData) {
    suspend fun getSchools() : NetworkState<List<School>> {
        val response = schoolDataService.getNycSchools(25)
        return response.parseResponse()
    }

}