package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.AdapterSchoolBinding
import com.example.myapplication.model.School

class SchoolAdapter: RecyclerView.Adapter<SchoolItemViewHolder>() {
    var schoolList = mutableListOf<School>()

    fun setSchools(schools: List<School>) {
        this.schoolList = schools.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterSchoolBinding.inflate(inflater, parent, false)
        return SchoolItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolItemViewHolder, position: Int) {
        val school = schoolList[position]
        holder.binding.schoolName.text = school.schoolName
    }

    override fun getItemCount(): Int {
        return schoolList.size
    }

}

class SchoolItemViewHolder(val binding: AdapterSchoolBinding) : RecyclerView.ViewHolder(binding.root) {

}