package com.example.myapplication
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.adapter.SchoolAdapter
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.repository.SchoolRepository
import com.example.myapplication.retrofit.NycSchoolData
import com.example.myapplication.retrofit.ServiceBuilder
import com.example.myapplication.viewmodel.MainViewModel
import com.example.myapplication.viewmodel.MainViewModelFactory


class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private val schoolAdapter = SchoolAdapter()

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val nycSchoolService = ServiceBuilder.buildService(NycSchoolData::class.java)
        val schoolRepository = SchoolRepository(nycSchoolService)
        binding.schoolList.adapter = schoolAdapter

        viewModel = ViewModelProvider(this, MainViewModelFactory(schoolRepository)).get(MainViewModel::class.java)

        viewModel.schoolList.observe(this) {
            Log.i("Schools from VM", viewModel.schoolList.toString())
            schoolAdapter.setSchools(it)
        }

        viewModel.getAllSchools()
    }
}







