package com.example.myapplication.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.repository.SchoolRepository

class MainViewModelFactory constructor(private val schoolRepository: SchoolRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            MainViewModel(this.schoolRepository) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}