package com.example.myapplication.model

import com.google.gson.annotations.SerializedName

data class School(
    @SerializedName("dbn") val databaseName: String,
    @SerializedName("school_name") val schoolName: String
)
