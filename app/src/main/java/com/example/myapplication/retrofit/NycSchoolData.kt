package com.example.myapplication.retrofit

import com.example.myapplication.model.School
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NycSchoolData {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getNycSchools(@Query("\$limit") limit: Int): Response<List<School>>
}